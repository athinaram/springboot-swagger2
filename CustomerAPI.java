package com.example.demo;

import java.util.List;

import javax.servlet.ServletContext;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ServletContextAware;

@RestController
public class CustomerAPI implements ServletContextAware {
	
	private ServletContext context;

	public void setServletContext(ServletContext servletContext) {
	     this.context = servletContext;
	}
	public CustomerDAO customerDao;
	
	@Autowired
	public CustomerAPI(CustomerDAO customerDao){
		this.customerDao=customerDao;
	}
	
	@RequestMapping(value = "/api/test", method = RequestMethod.GET)
	public String sayHello() {
	     return "Test Hello";
	}
	
	
	@GetMapping("/api/customers")
	public List<Customer> getAllCustomers(){
		
		return customerDao.list();
	}
	
	@GetMapping("/api/customers/{id}")
	public Customer get(@PathParam("id") int id) {

		return customerDao.get(id);
	}

	@PostMapping("/api/customers/create")
	public Customer create(@RequestBody Customer customer) {
		
		return customerDao.create(customer);
	}

	@DeleteMapping("/api/customers/delete/{id}")
	public int delete(@PathParam("id") int id) {
		
		return customerDao.delete(id);
	}

	@PostMapping("/api/customers/update/{id}")
	public Customer update(@PathParam("id") int id, @RequestBody Customer customer) {
		
		return customerDao.update(id, customer);
	}
}
