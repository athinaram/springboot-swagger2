package com.example.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CustomerDAO {

	// Dummy database. Initialize with some dummy values.
	private static List<Customer> customers(){
		return Arrays.asList(
				new Customer(1,"Siva", "Kumar", 123456789, 123456789, "Charlotte,NC"),
				new Customer(2,"Sony", "Seetha", 123456789, 123456789, "Charlotte,NC"),
				new Customer(3,"Sonu", "Seetha", 123456789, 123456789, "Charlotte,NC")
				);
	}
	

	/**
	 * Returns list of customers from dummy database.
	 * 
	 * @return list of customers
	 */
	public List list() {
		return customers();
	}

	/**
	 * Return customer object for given id from dummy database. If customer is
	 * not found for id, returns null.
	 * 
	 * @param id
	 *            customer id
	 * @return customer object for given id
	 */
	public Customer get(int id) {

		List<Customer> customers=customers();
		for (Customer c : customers) {
			if (c.getId()==id) {
				return c;
			}
		}
		return null;
	}

	/**
	 * Create new customer in dummy database. Updates the id and insert new
	 * customer in list.
	 * 
	 * @param customer
	 *            Customer object
	 * @return customer object with updated id
	 */
	public Customer create(Customer customer) {
		List<Customer> customers=customers();
		customers.add(customer);
		return customer;
	}

	/**
	 * Delete the customer object from dummy database. If customer not found for
	 * given id, returns null.
	 * 
	 * @param id
	 *            the customer id
	 * @return id of deleted customer object
	 */
	public int delete(int id) {
		List<Customer> customers=customers();
		for (Customer c : customers) {
			if (c.getId()==id) {
				customers.remove(c);
				return id;
			}
		}

		return (Integer) null;
	}

	/**
	 * Update the customer object for given id in dummy database. If customer
	 * not exists, returns null
	 * 
	 * @param id
	 * @param customer
	 * @return customer object with id
	 */
	public Customer update(int id, Customer customer) {
		List<Customer> customers=customers();
		for (Customer c : customers) {
			if (c.getId()==id) {
				customer.setId(c.getId());
				customers.remove(c);
				customers.add(customer);
				return customer;
			}
		}

		return null;
	}

}
